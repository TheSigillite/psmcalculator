import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	Button,
	Dimensions,
	TouchableOpacity
} from 'react-native';
import { throwStatement } from '@babel/types';


export default class App extends Component {
	
	
	constructor() {
		super()
		this.state = {
			resultText: "",
			hiddenText: "",
			calculated: '',
			orientation: 'portrait',
			currentnumber: ""
		}
		//this.opsymbols = [['AC','π'],['+','-'],['*','/'],['(',')'],['^','√']];
		this.opsymbols = [['AC','π','e'],['+','^','e^x'],['-','√','ln'],['*','(','log10'],['/',')','x!']];
		this.operands = ['AC','^','+','-','*','/'];
		this.portraitoperands = ['AC','+','-','*','/'];
		this.symbols = [[7,8,9],[4,5,6],[1,2,3],['.',0,'=']];
		
	}
	getOrientation = () =>{
  		if( this.refs.rootView ){
      		if( Dimensions.get('window').width < Dimensions.get('window').height ){
				this.setState({ orientation: 'portrait' });
				console.log(this.state.orientation)
      		}else{
			this.setState({ orientation: 'landscape' });
			console.log(this.state.orientation)
      		}
  		}
	}
	UNSAFE_componentWillMount(){
  		this.getOrientation();
  
 		 Dimensions.addEventListener( 'change', () =>{
    		this.getOrientation();
  		});
	}
	componentWillUnMount() {
		Dimensions.removeEventListener('change');
		}
	///
	///Checks for unexpected eol before eval runs
	///
	validate(){
		var text = this.state.resultText;
		switch(text.slice(-1)){
			case '+':
			case '-':
			case '*':
			case '(':
			case '^':
			case '√':
			case '/':
				return false
		}
		return true
	}
	factorial(n) {
		return (n != 1) ? n * this.factorial(n - 1) : 1;
	  }
	///
	///Uses Eval to calculate results of the equation
	///
	calculateResult(){
		//console.log(this.state.hiddenText)
		console.log(this.state.currentnumber)
		this.setState({
			calculated: eval(this.state.hiddenText)
		})
		this.setState({
			resultText: '',
			hiddenText: "",
			currentnumber: ""
		})
	}
	///
	///Handles presses on the Math Keypad
	///
	operate(symbol){
		switch (symbol){
			case 'AC':
				this.setState({
					resultText: '',
					hiddenText: "",
					currentnumber: ""
				});
				break;
			case '+':
			case '-':
			case '*':
			case '/':
				const lastChar = this.state.resultText.split('').pop();
				if(this.operands.indexOf(lastChar)>0) return
				if(this.state.resultText=='') return
				console.log(this.state.currentnumber)
				this.setState({
					resultText: this.state.resultText + symbol,
					hiddenText: this.state.hiddenText + symbol,
					currentnumber: ""
				});
				break;
			case ')':
			case '(':
				this.setState({
					resultText: this.state.resultText +symbol,
					hiddenText: this.state.hiddenText + symbol
				});
				break;
			case 'π':
				this.setState({
					resultText: this.state.resultText + 'π',
					hiddenText: this.state.hiddenText + 'Math.PI',
					currentnumber: ""
				});
				break;
			case '^':
				this.setState({
					resultText: this.state.resultText + symbol + '(',
					hiddenText: this.state.hiddenText.substring(0,this.state.hiddenText.length-this.state.currentnumber.length) + "Math.pow("+this.state.currentnumber+",",
					currentnumber: ""
				})
				console.log(this.state.hiddenText)
				break;
			case '√':
				this.setState({
					resultText: this.state.resultText + symbol + '(',
					hiddenText: this.state.hiddenText + 'Math.sqrt('
				})
				break;
			case 'e':
				this.setState({
					resultText: this.state.resultText + symbol,
					hiddenText: this.state.hiddenText + 'Math.E',
					currentnumber: ""
				})
				break
			case 'e^x':
				this.setState({
					resultText: this.state.resultText + 'e^(',
					hiddenText: this.state.hiddenText + 'Math.pow(Math.E,',
					currentnumber: ""
				})
				break;
			case 'ln':
				this.setState({
					resultText: this.state.resultText + 'ln(',
					hiddenText: this.state.hiddenText + 'Math.log(',
					currentnumber: ""
				})
				break;
			case 'log10':
				this.setState({
					resultText: this.state.resultText + 'log10(',
					hiddenText: this.state.hiddenText + 'Math.log10(',
					currentnumber: ""
				})
				break;
			case 'x!':
				this.setState({
					resultText: this.state.resultText + '!',
					hiddenText: this.state.hiddenText.substring(0,this.state.hiddenText.length-this.state.currentnumber.length) + 'this.factorial('+this.state.currentnumber+')',
					currentnumber: ""
				})
				console.log(this.state.hiddenText)
		}
	}
	///
	///Handles presses of numeric keypad and = sign
	///
	buttonPressed(text){
		if(text == '='){
			return this.validate() && this.calculateResult()
		}
		this.setState({
			resultText: this.state.resultText + text,
			hiddenText: this.state.hiddenText + text,
			currentnumber: this.state.currentnumber + text
		})
	}
	// TODO: wywalić z fora do map
	generateButton(symb){
		return(<TouchableOpacity key={symb} onPress={() => this.buttonPressed(symb)} style={styles.btn}>
		<Text style={styles.btnText}>{symb}</Text>
	</TouchableOpacity>);
	}
	generateActionButtons(op){
		return(<TouchableOpacity style={styles.btn} onPress={() => this.operate(op)}>
		<Text style={[styles.btnText, styles.white]}>{op}</Text>
	</TouchableOpacity>)
	}
	render() {
		///
		///Generate Numeric Keypad
		///
		//let rows = [];
		//let symbols = [[7,8,9],[4,5,6],[1,2,3],['.',0,'=']];
		/*
		for(let i = 0;i < 4; i++){
			let row = [];
			for(let j = 0;j < 3; j++){
				row.push(<TouchableOpacity key={symbols[i][j]} onPress={() => this.buttonPressed(symbols[i][j])} style={styles.btn}>
					<Text style={styles.btnText}>{symbols[i][j]}</Text>
				</TouchableOpacity>);
			}
			rows.push(<View key={i} style={styles.row}>{row}</View>);
		}
		*/
		///
		///Generate Maths keypad
		///
		/*
		let rowops = []
		for(let i = 0; i< 5; i++){
			let ops = []
				for(let j = 0; j < 3; j++){
				ops.push(<TouchableOpacity style={styles.btn} onPress={() => this.operate(this.opsymbols[i][j])}>
					<Text style={[styles.btnText, styles.white]}>{this.opsymbols[i][j]}</Text>
				</TouchableOpacity>)
				}
			rowops.push(<View style={styles.row}>{ops}</View>);
		}
		*/
	//<View style={styles.column}>{this.portraitoperands.map(operaton => this.generateActionButtons(operaton))}</View>
		/*
		let portraitops = []
		for(let i = 0; i< 5; i++){
			let ops = []
			for(let j = 0; j < 1; j++){
				ops.push(<TouchableOpacity style={styles.btn} onPress={() => this.operate(this.opsymbols[i][j])}>
					<Text style={[styles.btnText, styles.white]}>{this.opsymbols[i][j]}</Text>
				</TouchableOpacity>)
			}
			portraitops.push(<View style={styles.row}>{ops}</View>)
		}
		*/
		return (
			<View ref = "rootView" style={styles.container}>
				<View style={styles.result}>
					<Text style={styles.resultText}>{this.state.resultText}</Text>
				</View>
				<View style={styles.calculation}>
					<Text style={styles.calculationText}>{this.state.calculated}</Text>
				</View>
				<View style={styles.buttons}>
					<View style={styles.numbers}>
						{this.symbols.map(member => <View style={styles.row}>{member.map(num => this.generateButton(num))}</View>)}
					</View>
					<View style={(this.state.orientation=='portrait') ? styles.operations : styles.landscapeoperations}>
						{(this.state.orientation=='portrait')?
						 <View style={styles.column}>{this.portraitoperands.map(operaton => this.generateActionButtons(operaton))}</View> 
						: this.opsymbols.map(opgroup => <View style={styles.row}>{opgroup.map(operand => this.generateActionButtons(operand))}</View>)}
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	result: {
		flex: 2,
		backgroundColor: 'white',
		justifyContent: 'center',
		alignItems: 'flex-end'
	},
	calculation: {
		flex: 1,
		backgroundColor: 'grey',
		justifyContent: 'center',
		alignItems: 'flex-end'
	},
	buttons: {
		flex: 7,
		flexDirection: 'row'
	},
	numbers: {
		flex: 3,
		backgroundColor: '#3b3b3b'
	},
	operations: {
		flex: 1,
		//justifyContent: 'space-around',
		//alignItems: 'stretch',
		backgroundColor: '#d98200'
	},
	landscapeoperations: {
		flex: 2,
		backgroundColor: '#d98200'
	},
	row: {
		flexDirection: 'row',
		flex: 1,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	column: {
		flexDirection: 'column',
		flex: 1,
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	resultText: {
		fontSize: 30,
		color: 'black'
	},
	calculationText: {
		fontSize: 24,
		color: 'white'
	},
	btn: {
		flex: 1,
		alignItems: 'center',
		alignSelf: 'stretch',
		justifyContent: 'center'
	},
	btnText: {
		fontSize: 30,
		color: 'white'
	},
	white: {
		color: 'white'
	}
})